package com.gitlab.ipergenitsa;

import com.gitlab.ipergenitsa.Log;

public class Main {
    public static void main(String[] args) {
        Log log = new Log();
        System.out.println(log.info("first message"));
        System.out.println(log.debug("first message"));
    }
}
